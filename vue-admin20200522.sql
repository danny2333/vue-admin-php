/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : vue-admin

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 22/05/2020 18:17:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_admin
-- ----------------------------
DROP TABLE IF EXISTS `auth_admin`;
CREATE TABLE `auth_admin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录密码；sp_password加密',
  `tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户手机号',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录邮箱',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `sex` smallint(1) NOT NULL DEFAULT 0 COMMENT '性别；0：保密，1：男；2：女',
  `last_login_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `last_login_time` datetime(0) NOT NULL COMMENT '最后登录时间',
  `create_time` datetime(0) NOT NULL COMMENT '注册时间',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '用户状态 0：禁用； 1：正常 ；2：未验证',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_login_key`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_admin
-- ----------------------------
INSERT INTO `auth_admin` VALUES (1, 'admin', '14e1b600b1fd579f47433b88e8d85291', 'admin', 'lmxdawn@gmail.com', 'sssss', 0, '127.0.0.1', '2020-05-22 11:27:39', '2018-07-06 17:19:00', 1);
INSERT INTO `auth_admin` VALUES (2, 'danny', '14e1b600b1fd579f47433b88e8d85291', '', '', '', 0, '127.0.0.1', '2020-05-22 11:44:37', '2020-05-19 16:14:32', 1);
INSERT INTO `auth_admin` VALUES (3, 'peter', '14e1b600b1fd579f47433b88e8d85291', '', '', '', 0, '127.0.0.1', '2020-05-20 14:56:25', '2020-05-20 14:32:36', 1);

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `role_id` int(11) UNSIGNED NOT NULL COMMENT '角色',
  `permission_rule_id` int(11) NOT NULL DEFAULT 0 COMMENT '权限id',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限规则分类，请加应用前缀,如admin_'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限授权表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, 4, 'admin');
INSERT INTO `auth_permission` VALUES (1, 3, 'admin');
INSERT INTO `auth_permission` VALUES (1, 2, 'admin');
INSERT INTO `auth_permission` VALUES (1, 1, 'admin');
INSERT INTO `auth_permission` VALUES (2, 11, 'admin');
INSERT INTO `auth_permission` VALUES (2, 7, 'admin');
INSERT INTO `auth_permission` VALUES (2, 2, 'admin');
INSERT INTO `auth_permission` VALUES (2, 1, 'admin');
INSERT INTO `auth_permission` VALUES (2, 16, 'admin');

-- ----------------------------
-- Table structure for auth_permission_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission_rule`;
CREATE TABLE `auth_permission_rule`  (
  `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '规则编号',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '父级id',
  `name` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态：为1正常，为0禁用',
  `condition` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  `listorder` int(10) NOT NULL DEFAULT 0 COMMENT '排序，优先级，越小优先级越高',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission_rule
-- ----------------------------
INSERT INTO `auth_permission_rule` VALUES (1, 0, 'user_manage', '用户管理', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (2, 1, 'user_manage/admin_manage', '管理组', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (3, 2, 'api/admin/index', '管理员管理', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (4, 3, 'api/admin/save', '添加管理员', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (5, 3, 'api/admin/edit', '编辑管理员', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (6, 3, 'api/admin/delete', '删除管理员', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (7, 2, 'api/role/index', '角色管理', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (8, 7, 'api/role/save', '添加角色', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (9, 7, 'api/role/edit', '编辑角色', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (10, 7, 'api/role/delete', '删除角色', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (11, 7, 'api/role/auth', '角色授权', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (12, 2, 'api/permission/index', '权限管理', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (13, 12, 'api/permission/save', '添加权限', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (14, 12, 'api/permission/edit', '编辑权限', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (15, 12, 'api/permission/delete', '删除权限', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');
INSERT INTO `auth_permission_rule` VALUES (16, 7, 'api/role/authList', '授权列表', 1, '', 999, '2018-07-06 17:19:00', '2018-07-06 17:19:00');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父角色ID',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `listorder` int(3) NOT NULL DEFAULT 0 COMMENT '排序，优先级，越小优先级越高',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES (1, '超级管理员', 0, 1, '拥有网站最高管理员权限！', '2018-07-06 17:19:00', '2018-07-06 17:19:00', 0);
INSERT INTO `auth_role` VALUES (2, '客户', 0, 1, '客户', '2020-05-20 09:48:54', '2020-05-20 09:48:54', 0);

-- ----------------------------
-- Table structure for auth_role_admin
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_admin`;
CREATE TABLE `auth_role_admin`  (
  `role_id` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '角色 id',
  `admin_id` int(11) NULL DEFAULT 0 COMMENT '管理员id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色对应表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_role_admin
-- ----------------------------
INSERT INTO `auth_role_admin` VALUES (1, 1);
INSERT INTO `auth_role_admin` VALUES (2, 2);
INSERT INTO `auth_role_admin` VALUES (1, 3);
INSERT INTO `auth_role_admin` VALUES (2, 3);

-- ----------------------------
-- Table structure for file_resource
-- ----------------------------
DROP TABLE IF EXISTS `file_resource`;
CREATE TABLE `file_resource`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资源id',
  `tag_id` int(11) NOT NULL DEFAULT 0 COMMENT '资源分组id',
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT '资源的类型（0：图片）',
  `filename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源的原名',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源的路径（不加 域名的地址）',
  `size` int(11) NOT NULL DEFAULT 0 COMMENT '大小',
  `ext` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源的文件后缀',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of file_resource
-- ----------------------------
INSERT INTO `file_resource` VALUES (1, 1, 0, 'Group 5.png', 'resources/20200519/a335570a5516eba27feb02eb7cc391df.png', 7539, 'png', '2018-05-30 20:41:54');
INSERT INTO `file_resource` VALUES (12, 0, 0, '401.f85649d8.png', 'resources/20200522/9350d8263e18959e73c0e4709cd1aca7.png', 14928, 'png', '2020-05-22 18:06:49');
INSERT INTO `file_resource` VALUES (13, 0, 0, 'ttsloading.gif', 'resources/20200522/4366499ec3978a8a31e3445deb05c68d.gif', 891466, 'gif', '2020-05-22 18:09:56');

-- ----------------------------
-- Table structure for file_resource_tag
-- ----------------------------
DROP TABLE IF EXISTS `file_resource_tag`;
CREATE TABLE `file_resource_tag`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '资源分组的id',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '资源分组的tag',
  `create_time` datetime(0) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '资源的分组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of file_resource_tag
-- ----------------------------
INSERT INTO `file_resource_tag` VALUES (1, '测试', '2018-05-30 20:41:48');
INSERT INTO `file_resource_tag` VALUES (2, 'test', '2020-05-21 09:56:56');
INSERT INTO `file_resource_tag` VALUES (3, '666', '2020-05-21 10:01:07');

SET FOREIGN_KEY_CHECKS = 1;
