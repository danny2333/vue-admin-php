<?php

namespace app\http\middleware;

use think\Response;
use app\common\exception\JsonException;

class PermissionAuth
{
    public function handle($request, \Closure $next)
    {
        if (empty($request->userData)) {
            returnMsg('', 1, '请登录');
        }

        $tll = $request->controller();
        $act = $request->action();
        $action = ['login', 'vertify'];
        if (in_array($act, $action)) {
            return $next($request);
        }
        if ($tll == 'Index' && $act == 'index') {
            return $next($request);
        }

        $access_token = $request->param('access_token');

        $decoded = Jswt::decode($access_token);

        # 获取当前的请求方法
        # 当数据库中没有添加这个权限的时候, 就放开通行
        $rule = $request->pathinfo();
        $method = strtolower($request->method());

        # 匹配前 : admin/superstore/online/293
        # 匹配过滤后: admin/superstore/online/<id>

        $rule = $this->filter_rule($rule);

        if (is_array($decoded)) {
            if ($decoded['id'] != Config::get('auth.auth_super_id')) {
                $user = AdminUser::find($decoded['id']);
                $request->userData = $user;
                if ($permission = Permission::where('rule', $rule)
                    ->where('method', 'like', "%$method%")
                    ->find()
                ) {
                    if (!$request->userData->can($permission)) {
                        returnMsg('', 1, '没有权限');
                    }
                } else {
                    returnMsg('', 1, '没有权限');
                }
            }

            return $next($request);
        } else {
            $result = [
                'code' => 1001,
                'msg'  => $decoded,
                'time' => time(),
                'data' => [],
            ];
            return Response::create($result, 'json');
        }
    }

    /**
     * 过滤路由
     */
    public function filter_rule($rule)
    {
        $pattern = '/(.*)\/([0-9]+)$/';
        $replacement = "$1/<id>";
        return preg_replace($pattern, $replacement, $rule);
    }
}
