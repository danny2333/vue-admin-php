<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// Route::rule('/api/danny/test', 'admin/Index/index');
# 后台管理
Route::group('api', function () {

    // Auth
    Route::get('admin/index', 'admin/auth.Admin/index');    //管理员管理
    Route::get('admin/roleList', 'admin/auth.Admin/roleList');  //全部角色
    Route::post('admin/save', 'admin/auth.Admin/save'); //添加管理员
    Route::post('admin/edit', 'admin/auth.Admin/edit'); //编辑管理员
    Route::delete('admin/delete', 'admin/auth.Admin/delete');   //删除管理员

    // Login
    Route::post('auth/login', 'admin/auth.Login/index');    //登录
    Route::get('auth/userInfo', 'admin/auth.Login/userInfo');   //获取登录用户信息
    Route::post('auth/out', 'admin/auth.Login/out');    //登出
    Route::post('auth/password', 'admin/auth.Login/password');  //修改密码

    // Permission
    Route::get('permission/index', 'admin/auth.PermissionRule/index');  //权限管理
    Route::post('permission/save', 'admin/auth.PermissionRule/save');   //添加权限
    Route::post('permission/edit', 'admin/auth.PermissionRule/edit');   //编辑权限
    Route::delete('permission/delete', 'admin/auth.PermissionRule/delete'); //删除权限

    // Role
    Route::get('role/index', 'admin/auth.Role/index');  //角色管理
    Route::get('role/authList', 'admin/auth.Role/authList');    //授权列表
    Route::post('role/auth', 'admin/auth.Role/auth');   //角色授权
    Route::post('role/save', 'admin/auth.Role/save');   //添加角色
    Route::post('role/edit', 'admin/auth.Role/edit');   //编辑角色
    Route::delete('role/delete', 'admin/auth.Role/delete'); //删除角色

    // FileResource
    Route::get('file/resource/index', 'admin/file.Resource/index');
    Route::post('file/resource/add', 'admin/file.Resource/add');

    // ResourceTag
    Route::get('file/resource_tag/index', 'admin/file.ResourceTag/index');
    Route::post('file/resource_tag/add', 'admin/file.ResourceTag/add');

    // Upload
    Route::get('file/qiuNiuUpToken', 'admin/file.Upload/qiuNiuUpToken');
    Route::post('file/createFile', 'admin/file.Upload/createFile');

    Route::get('test', 'admin/Index/index');
})->allowCrossDomain();
